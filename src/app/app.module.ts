import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {FlexLayoutModule} from '@angular/flex-layout';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatButtonModule, MatCheckboxModule} from '@angular/material';
import { HomeComponent } from './home/home.component';
import { StartComponent } from './start/start.component';
import { RouterModule } from '@angular/router';
import { FirstComponent } from './home/first/first.component';
import { SecondComponent } from './home/second/second.component';
import { ThirdComponent } from './home/third/third.component';
import {MatCardModule} from '@angular/material/card';
import { HomeFirstComponent } from './home/first/home-first/home-first.component';
import { HomeSecondComponent } from './home/first/home-second/home-second.component';
import {MatToolbarModule} from '@angular/material/toolbar';
import { TestSecondComponent } from './home/second/test-second/test-second.component';
import { Test2SecondComponent } from './home/second/test2-second/test2-second.component';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {MatSelectModule} from '@angular/material/select';
import { Final1Component } from './home/third/final1/final1.component';
import { Final2Component } from './home/third/final2/final2.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    StartComponent,
    FirstComponent,
    SecondComponent,
    ThirdComponent,
    HomeFirstComponent,
    HomeSecondComponent,
    TestSecondComponent,
    Test2SecondComponent,
    Final1Component,
    Final2Component
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FlexLayoutModule,
    BrowserAnimationsModule,
    MatCheckboxModule,
    MatButtonModule,
    RouterModule,
    MatToolbarModule,
    MatCardModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
