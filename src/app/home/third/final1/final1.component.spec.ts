import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Final1Component } from './final1.component';

describe('Final1Component', () => {
  let component: Final1Component;
  let fixture: ComponentFixture<Final1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Final1Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Final1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
