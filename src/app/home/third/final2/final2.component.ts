import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-final2',
  templateUrl: './final2.component.html',
  styleUrls: ['./final2.component.css']
})
export class Final2Component implements OnInit {
  id: number;
  constructor(private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.params.subscribe(id => {
      console.log('Hey you router to a page with the value :', id);
      this.id = id.id;
    });
  }

}
