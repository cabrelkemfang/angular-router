import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-test2-second',
  templateUrl: './test2-second.component.html',
  styleUrls: ['./test2-second.component.css']
})
export class Test2SecondComponent implements OnInit {

  constructor(public router: Router) { }

  ngOnInit() {
    console.log('you call me, now i am there ......');

  }

}
