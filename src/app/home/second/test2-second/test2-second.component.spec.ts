import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Test2SecondComponent } from './test2-second.component';

describe('Test2SecondComponent', () => {
  let component: Test2SecondComponent;
  let fixture: ComponentFixture<Test2SecondComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Test2SecondComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Test2SecondComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
