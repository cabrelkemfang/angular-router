import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-first',
  templateUrl: './first.component.html',
  styleUrls: ['./first.component.css']
})
export class FirstComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }

  goToSup1() {
    this.router.navigateByUrl('/home/(first:first/first-first//second:second//third:third)');
  }

  goToSup2() {
    this.router.navigateByUrl('/home/(first:first/first-second//second:second//third:third)');
  }
}
