import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home-second',
  templateUrl: './home-second.component.html',
  styleUrls: ['./home-second.component.css']
})
export class HomeSecondComponent implements OnInit {

  constructor(public router: Router) { }

  ngOnInit() {
  }
  go() {
    this.router.navigateByUrl('/home/(first:first/first-first//second:second/test1//third:third)');
  }

  go1() {
    this.router.navigateByUrl('/home/(first:first/first-first//second:second/test//third:third/final/45)');
  }
}
