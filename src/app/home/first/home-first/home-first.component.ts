import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home-first',
  templateUrl: './home-first.component.html',
  styleUrls: ['./home-first.component.css']
})
export class HomeFirstComponent implements OnInit {

  constructor(public router: Router) { }

  ngOnInit() {
  }

  go() {
    this.router.navigateByUrl('/home/(first:first/first-first//second:second/test1//third:third)');
  }

  go1() {
    this.router.navigateByUrl('/home/(first:first/first-first//second:second/test//third:third/final/45)');
  }
}
