import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { StartComponent } from './start/start.component';
import { HomeComponent } from './home/home.component';
import { FirstComponent } from './home/first/first.component';
import { SecondComponent } from './home/second/second.component';
import { ThirdComponent } from './home/third/third.component';
import { HomeFirstComponent } from './home/first/home-first/home-first.component';
import { HomeSecondComponent } from './home/first/home-second/home-second.component';
import { TestSecondComponent } from './home/second/test-second/test-second.component';
import { Test2SecondComponent } from './home/second/test2-second/test2-second.component';
import { Final1Component } from './home/third/final1/final1.component';
import { Final2Component } from './home/third/final2/final2.component';


const routes: Routes = [
  { path: '', redirectTo: 'start', pathMatch: 'full' },
  { path: 'start', component: StartComponent },
  {
    path: 'home', component: HomeComponent,
    children: [
      // { path: '', redirectTo: 'first', pathMatch: 'full' },
      {
        path: 'first', component: FirstComponent, outlet: 'first',
        children: [
          { path: '', redirectTo: 'first-first', pathMatch: 'full' },
          { path: 'first-first', component: HomeFirstComponent },
          { path: 'first-second', component: HomeSecondComponent }
        ]
      },
      {
        path: 'second', component: SecondComponent, outlet: 'second',
        children: [
          { path: '', redirectTo: 'test', pathMatch: 'full' },
          { path: 'test', component: TestSecondComponent },
          { path: 'test1', component: Test2SecondComponent },

        ]
      },
      {
        path: 'third', component: ThirdComponent, outlet: 'third',
        children: [
          { path: '', pathMatch: 'full', redirectTo: 'final' },
          { path: 'final', component: Final1Component },
          { path: 'final/:id', component: Final2Component }
        ]
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
